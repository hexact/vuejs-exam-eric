import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate";

import products from '@/assets/products.json'

Vue.use(Vuex)

export default new Vuex.Store({
   plugins: [createPersistedState()],
   state: {
      allProducts: [],

   },
   getters: {
      getAllProducts: state => state.allProducts,
      getProductId: state => id => state.allProducts.filter(item => id == item.productId)
   },

   mutations: {
      FETCHING_PRODUCTS (state, payload) {
         state.allProducts = payload
      },
      UPDATE_PRODUCT (state, payload) {
         const { id, data } = payload
         state.allProducts.map(item => {
            if (item.productId == id) {
               item.productName = data.productName
               item.productShortDescription = data.productShortDescription
            }
         })
      }
   },
   actions: {
      fetchAllProducts ({ commit }) {
         setTimeout(() => {
            return new Promise((resolve, reject) => {

               if (products) {
                  commit('FETCHING_PRODUCTS', products)
                  return resolve(products)
               }
            })
         }, 750);
      },
      updateProduct ({ commit }, payload) {
         commit('UPDATE_PRODUCT', payload)
      }
   },

})
