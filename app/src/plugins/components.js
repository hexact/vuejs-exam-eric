import Vue from 'vue'
import FetchAllProducts from '@/components/DataProvider/FetchAllProducts'
import ProductListing from '@/components/product/ProductListing'
import ImageIcon from '@/components/Utilities/ImageIcon'
const components = {
   FetchAllProducts,
   ProductListing,
   ImageIcon
}

export default Vue.use({
   install: Vue => {
      Object.keys(components).forEach(key => {
         Vue.component(key, components[key])
      })
   }
})
