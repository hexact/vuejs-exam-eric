import Vue from 'vue'
import Router from 'vue-router'
import store from '@/store'

Vue.use(Router)

export default new Router({
   mode: 'history',
   routes: [
      {
         path: '/',
         name: 'home',
         component: () => import(/* webpackChunkName: "home" */ './views/Home.vue'),
      },
      {
         path: '/product/:productId',
         name: 'product',
         component: () => import(/* webpackChunkName: "product" */ './views/Product.vue'),
         beforeEnter (to, from, next) {
            const productId = to.params.productId
            const getProductId = store.getters['getProductId'](productId)
            if (productId && getProductId.length > 0) {
               next();
            } else {
               next({ name: 'PageNotFound', params: { errorMessage: 'Sorry! product not found.' } })
            }
         },
      },
      {
         path: '/404',
         alias: '*',
         props: true,
         name: 'PageNotFound',
         component: () => import(/* webpackChunkName: "product" */ './views/PageNotFound.vue'),
      }
   ]
})
